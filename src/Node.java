import java.util.*;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }

    static Node parsePostfix(String s) {
        if (s.contains(",,")) {
            throw new RuntimeException("Double commas in string: " + s);
        } else if (s.contains("\t")) {
            throw new RuntimeException("TAB in string " + s);
        } else if (s.contains("()")) {
            throw new RuntimeException("Empty subtree in string " + s);
        } else if (s.contains(" ")) {
            throw new RuntimeException("Space in string " + s);
        } else if (s.contains("((") && s.contains("))")) {
            throw new RuntimeException("String " + s + "has double brackets in it.");
        } else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
            throw new RuntimeException("Two roots in string " + s);
        }
        String str = s.replaceAll("", " ");
        StringTokenizer st = new StringTokenizer(str, " ");
        StringTokenizer st2 = new StringTokenizer(str, " ");

        st2.nextElement();
        Stack<Node> stack = new Stack();

        Node node = new Node(null, null, null);
        boolean isRoot = false;
        while (st.hasMoreElements()) {
            String nextElement = null;
            if (st2.hasMoreElements()) {
                nextElement = st2.nextToken();
            }
            String element = st.nextToken();
            if (element.equals("(")) {
                if (isRoot) {
                    throw new RuntimeException("You are trying to replace the root.");
                }
                stack.push(node);
                node.firstChild = new Node(null, null, null);
                node = node.firstChild;

                if (nextElement.equals(",")) {
                    throw new RuntimeException("Comma after node in " + s);
                }
            } else if (element.equals(")")) {

                node = stack.pop();
                if (stack.size() == 0) {
                    isRoot = true;
                }
            } else if (element.equals(",")) {

                if (isRoot) {
                    throw new RuntimeException("You are trying to replace the root.");
                }
                node.nextSibling = new Node(null, null, null);
                node = node.nextSibling;
            } else {
                if (node.name == null) {
                    node.name = element;
                } else {
                    node.name += element;
                }
            }
        }
        return node;
    }

    String leftParentheticRepresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        if (firstChild != null) {
            sb.append("(");
            sb.append(firstChild.leftParentheticRepresentation());
            sb.append(")");
        }

        if (nextSibling != null) {
            sb.append(",");
            sb.append(nextSibling.leftParentheticRepresentation());
        }
        return sb.toString();
    }

    public static void main(String[] param) {
        String s = "(B)A";
        Node t = Node.parsePostfix(s);
        String v = t.leftParentheticRepresentation();
        System.out.println("  " + s + " is equal " + v);
    }
}

